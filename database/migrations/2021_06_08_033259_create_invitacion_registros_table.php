<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitacionRegistrosTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('invitacion_registros', function (Blueprint $table) {
      $table->id();
      $table->integer('usuario_id')->nullable();
      $table->integer('usuario_detail_id')->nullable();
      $table->integer('vendedor_id');
      $table->integer('profile_id');
      $table->string('email_referido');
      $table->boolean('is_register_ok');
      $table->uuid('verification_hash')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('invitacion_registros');
  }
}
