<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentoIdentidadsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('documentos_identidad', function (Blueprint $table) {
      $table->id();
      $table->integer('usuario_id');
      $table->integer('usuario_detail_id');
      $table->string('doc_id_front_photo')->nullable();
      $table->string('doc_id_reverse_photo')->nullable();
      $table->timestamp('verified_at')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('documentos_identidad');
  }
}
