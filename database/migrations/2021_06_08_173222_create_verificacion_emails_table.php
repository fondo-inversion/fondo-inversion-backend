<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVerificacionEmailsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('verificacion_emails', function (Blueprint $table) {
      $table->id();
      $table->string('email');
      $table->integer('expired_at');
      $table->string('verification_code')->nullable();
      $table->string('verification_hash')->nullable();
      $table->boolean('is_used');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('verificacion_emails');
  }
}
