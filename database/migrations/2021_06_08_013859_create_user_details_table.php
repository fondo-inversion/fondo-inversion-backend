<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('user_details', function (Blueprint $table) {
      $table->id();
      $table->integer('usuario_id');
      $table->string('photo')->nullable();
      $table->string('email');
      $table->integer('profile_id');
      $table->integer('verificacion_email_id')->nullable();
      $table->integer('documento_identidad_id')->nullable();
      $table->integer('invitacion_registro_id')->nullable();
      $table->integer('register_at')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('user_details');
  }
}
