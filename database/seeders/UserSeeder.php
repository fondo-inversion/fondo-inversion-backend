<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use App\Services\CreateSuperAdmin\CreateSuperAdmin;

class UserSeeder extends Seeder
{
    public function __construct(CreateSuperAdmin $createSuperAdmin)
    {
      $this->createSuperAdmin = $createSuperAdmin;
    }
    
    public function run()
    {
      $this->createSuperAdmin->create();
    }
}
