<?php

namespace Tests\Unit;

use App\Mail\InvitacionRegistroMail;
use App\Models\DocumentoIdentidad;
use App\Models\InvitacionRegistro;
use App\Models\Profile;
use Illuminate\Support\Facades\App;
use Tests\TestCase;
use App\Services\ProfileCrud\ProfileCrud;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Services\VerificacionEmail\VerficacionEmailService;
use App\Services\InvitacionRegistro\InvitacionRegistroService;
use App\Models\User;
use App\Models\VerificacionEmail;
use Carbon\Carbon;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testProfileCreate()
    {
      $profileCrud = $this->app->make(ProfileCrud::class);
      $profile = $this->makeProfile($profileCrud);
      $profileFindTest = $profileCrud->find($profile->id);
      dd($profileFindTest);
    }

    public function testProfileList()
    {
      $profileCrud = $this->app->make(ProfileCrud::class);
      $this->makeProfile($profileCrud);
      $this->makeProfile($profileCrud);
      $this->makeProfile($profileCrud);

      $profileList = $profileCrud->list();

      dd($profileList->toArray());
    }

    public function testVerificationEmail()
    {
      $verficacionEmailService = $this->app->make(VerficacionEmailService::class);
      $now = Carbon::now();
      $unMinutoDespues = $now->copy()->addMinutes(1);
      $expiredAt = $now->copy()->addMinutes(3);
      $unosSegundoDespues = $expiredAt->copy()->addSeconds(1);

      $verficacionEmail = $verficacionEmailService->send(
        'naiger67@gmail.com', 
        $expiredAt->timestamp, 
        function($verifcationEmail) {return $verifcationEmail;
      });

      $response2 = $verficacionEmailService->validation(
        $verficacionEmail->verificationHash(), 
        $verficacionEmail->verificationCode(), 
        $unMinutoDespues->timestamp
      );

      $verficacionEmail = $verficacionEmailService->send(
        'naiger67@gmail.com', 
        $expiredAt->timestamp, 
        function($verifcationEmail) {return $verifcationEmail;}
      );

      $response2 = $verficacionEmailService->validation(
        $verficacionEmail->verificationHash(), 
        $verficacionEmail->verificationCode(), 
        $unMinutoDespues->timestamp
      );

      $verficacionEmail = $verficacionEmailService->send(
        'naiger67@gmail.com', 
        $expiredAt->timestamp, 
        function($verifcationEmail) {return $verifcationEmail;}
      );

      $response2 = $verficacionEmailService->validation(
        $verficacionEmail->verificationHash(), 
        $verficacionEmail->verificationCode(), 
        $unMinutoDespues->timestamp
      );

      dd($response2->toArray());
    }
    

    protected function makeProfile($profileCrud)
    {
      $command = collect([
        'name'=>'PERMISO_A',
        'label'=>'Permiso A',
        'permissionJsonFilePath'=>'C:\xampp\htdocs\cbfx-api\tests\Unit\permisos.json'
      ]);
      $profile = $profileCrud->create($command);
      return $profile;
    }
}
