<?php

namespace Tests\Unit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Services\Register\RegisterUserStandar;
use App\Services\CreateSuperAdmin\CreateSuperAdmin;
use App\Services\InvitacionRegistro\InvitacionRegistroFactory;
use App\Services\RecuperarPassword\RecuperarPassword;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use App\Services\InvitacionRegistro\RegisterUserFromInvitacion;

class CBFXTest extends TestCase
{
  use DatabaseTransactions;

  public function testCreateSuperAdmin()
  {
    $CreateSuperAdmin = $this->app->make(CreateSuperAdmin::class);
    $context = $CreateSuperAdmin->create();
    dd($context);
  }

  public function testRecuperarPassword()
  {
    $CreateSuperAdmin = $this->app->make(CreateSuperAdmin::class);
    $context = $CreateSuperAdmin->create();
    $usuarioDetail = $context['admin'];
    $user = $usuarioDetail->usuario;
    $RecuperarPassword = $this->app->make(RecuperarPassword::class);
    $verification = $RecuperarPassword->verificationEmail($user->email);
    $user = $RecuperarPassword->recover($verification->verificationHash(), $verification->verificationCode(), 'minuevapass');  
    dd($user->validateForPassportPasswordGrant('minuevapass'));
  }

  public function testInvitacionRegistroFactory()
  {
    $CreateSuperAdmin = $this->app->make(CreateSuperAdmin::class);
    $context = $CreateSuperAdmin->create();
    $usuarioDetail = $context['admin'];
    $user = $usuarioDetail->usuario;
    $profiles = $context['profiles'];
    $profileVendedor = $profiles->get(1);

    $InvitacionRegistroFactory = $this->app->make(InvitacionRegistroFactory::class);
    $email = 'naiger90@gmail.com';
    $registerId = $user->id;
    $profileId = $profileVendedor->id;
    $verficationHash = Uuid::generate()->string;
    $verficationHash2 = Uuid::generate()->string;

    $invitacionRegistro1 = $InvitacionRegistroFactory->make($email, $registerId, $profileId, $verficationHash);
    //$invitacionRegistro1->is_register_ok = true;
    //$invitacionRegistro1->save();
    $invitacionRegistro2 = $InvitacionRegistroFactory->make($email, $registerId, $profileId, $verficationHash2);

    dd([$invitacionRegistro1->toArray(), $invitacionRegistro2->toArray()]);
  }

  public function testRegisterUserFromInvitacion()
  {
    $CreateSuperAdmin = $this->app->make(CreateSuperAdmin::class);
    $context = $CreateSuperAdmin->create();
    $usuarioDetail = $context['admin'];
    $user = $usuarioDetail->usuario;
    $profiles = $context['profiles'];
    $profileVendedor = $profiles->get(1);

    $InvitacionRegistroFactory = $this->app->make(InvitacionRegistroFactory::class);
    $email = 'naiger90@gmail.com';
    $registerId = $user->id;
    $profileId = $profileVendedor->id;
    $verficationHash = Uuid::generate()->string;
    $verficationHash2 = Uuid::generate()->string;

    $invitacionRegistro = $InvitacionRegistroFactory->make($email, $registerId, $profileId, $verficationHash);
    $password = '1234567890';
    $registerAt = Carbon::now();

    $RegisterUserFromInvitacion = $this->app->make(RegisterUserFromInvitacion::class);
    $usuarioDetail = $RegisterUserFromInvitacion->register($invitacionRegistro->verificationHash(), $password, $registerAt->timestamp);
    //$usuarioDetail = $RegisterUserFromInvitacion->register($invitacionRegistro->verificationHash(), $password, $registerAt->timestamp);

    $validPass = $usuarioDetail->usuario->validateForPassportPasswordGrant('1234567890');
    //dd($usuarioDetail->toArray());
    dd($validPass);
  }
}
