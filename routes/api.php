<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\InvitacionRegistroController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\RegistroController;

Route::post('login', [LoginController::class, 'login']);
Route::post('verificar-registro-cuenta', [RegistroController::class, 'verificacionEmail']);
Route::post('registrar-cuenta', [RegistroController::class, 'register']);

Route::middleware(['auth:api'])->group(function () {
  Route::post('logout', [LogoutController::class, 'logout']);
  Route::post('logout-all', [LogoutController::class, 'logoutAll']);

  Route::prefix('invitacion-registro')->group(function () {
    Route::post('create', [InvitacionRegistroController::class, 'create']);
  });  

  Route::prefix('profiles')->group(function () {
    Route::get('cache', [ProfileController::class, 'cache']);
    Route::post('create', [ProfileController::class, 'create']);
    Route::get('detail', [ProfileController::class, 'detail']);
    Route::get('list', [ProfileController::class, 'list']);
    Route::get('permissions', [ProfileController::class, 'permissions']);
    Route::post('refresh', [ProfileController::class, 'refresh']);
    Route::post('update', [ProfileController::class, 'update']);
  }); 
});