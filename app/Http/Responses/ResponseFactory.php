<?php

namespace App\Http\Responses;

class ResponseFactory
{
 
  public static function ok($data)
  {
    return  response()->json([
      'data'=>$data,
      'code'=>1000,
      'message'=>''
    ], 200);
  }

  public static function err($code, $message)
  {
    return  response()->json([
      'data'=>null,
      'code'=>$code,
      'message'=>$message
    ], 500);
  }
}