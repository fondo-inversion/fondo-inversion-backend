<?php

namespace App\Http\Controllers;

use App\Http\Responses\ResponseFactory;
use Illuminate\Http\Request;
use App\Services\ProfileCrud\ProfileCrud;
use \Webpatser\Uuid\Uuid;
use App\Services\ProfileCrud\PermissionProfile;
use Exception;

class ProfileController extends Controller
{
  protected $profileCrud;
  protected $permissionProfile;
  public function __construct(ProfileCrud $profileCrud, PermissionProfile $permissionProfile)
  {
    $this->profileCrud = $profileCrud;
    $this->permissionProfile = $permissionProfile;
  }

  public function create(Request $request)
  {
    try {
      $permissionJson = $request->file('permissions');
      $hashCode = Uuid::generate()->string;
      $permissions = $this->permissionsEncode($request);

      $profile = $this->profileCrud->create(
        $request->get('name'),
        $request->get('label'),
        $hashCode,
        $request->get('mainPage'),
        $permissions
      );
      $profileCache = $this->profileCrud->cache();
      return ResponseFactory::ok($profileCache);
    } catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  public function update(Request $request)
  {
    try {
      $hashCode = Uuid::generate()->string;
      $permissions = $this->permissionsEncode($request);

      $profile = $this->profileCrud->update(
        $request->get('id'),
        $request->get('name'),
        $request->get('label'),
        $hashCode,
        $request->get('mainPage', ''),
        $permissions
      );
      $profileCache = $this->profileCrud->cache();
      return ResponseFactory::ok($profileCache);
    } catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  public function cache()
  {
    return $this->profileCrud->cache();
  }

  public function refresh()
  {
    try {
      $profileCache = $this->profileCrud->refresh();
      return ResponseFactory::ok($profileCache);
    } catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  public function list()
  {
    try {
      $profiles = $this->profileCrud->list();
      return ResponseFactory::ok($profiles);
    } catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  public function detail(Request $request)
  {
    try {
      $profileX = $this->profileCrud->detail($request->get('id'));
      return ResponseFactory::ok($profileX);
    } catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  public function permissions(Request $request)
  {
    try {
      $profileUser = $this->profileUser($request);
      $permissions = $this->profileCrud->getPermissions($profileUser->id);
      return ResponseFactory::ok($permissions);
    } catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  protected function profileUser(Request $request)
  {
    return $request->user()->userDetail->profile;
  }

  protected function permissionsEncode(Request $request)
  {
    $permissionJson = $request->file('permissions');
    if ($permissionJson)
      return $this->permissionProfile->encodePermissionFromJsonFile($permissionJson->getRealPath());

    return '';
  }
}
