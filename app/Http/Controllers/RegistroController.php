<?php

namespace App\Http\Controllers;

use App\Http\Responses\ResponseFactory;
use Illuminate\Http\Request;
use App\Services\Register\RegisterUserStandar;
use App\Mail\RegisterUserMail;
use Illuminate\Support\Facades\Mail;
use Exception;

class RegistroController extends Controller
{
  protected $registerUserStandar;

  public function __construct(RegisterUserStandar $registerUserStandar)
  {
    $this->registerUserStandar = $registerUserStandar;
  }

  public function verificacionEmail(Request $request)
  {
    try {
      $email = $request->get('email');
      $verificationHash = $this->registerUserStandar->verificarEmail($email, function ($verficacionEmail) {
        Mail::to($verficacionEmail->email())->send(new RegisterUserMail($verficacionEmail->verificationCode()));
        return $verficacionEmail->verificationHash();
      });
      return ResponseFactory::ok($verificationHash);
    } 
    
    catch (Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }

  public function register(Request $request)
  {
    try {

      $userDetail = $this->registerUserStandar->register(
        $request->get('verificationHash'),
        $request->get('verificactionCode'),
        $request->get('password')
      );
  
      return ResponseFactory::ok($userDetail);
    }

    catch(Exception $e) {
      return ResponseFactory::err(1001, $e->getMessage());
    }
  }
}
