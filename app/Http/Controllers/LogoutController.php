<?php

namespace App\Http\Controllers;

use App\Http\Responses\ResponseFactory;
use Illuminate\Http\Request;
use App\Services\Logout\LogoutService;
use Exception;

class LogoutController extends Controller
{
  protected $logoutService;

  public function __construct(LogoutService $logoutService)
  {
    $this->logoutService = $logoutService;
  }

  public function logout(Request $request) 
  {
    try {
      $res = $this->logoutService->logout($request->user());
      return ResponseFactory::ok($res);
    } catch(Exception $e) {
      return ResponseFactory::err(1001, 'Ha ocurrido un error');
    }
  }

  public function logoutAll(Request $request) 
  {
    try {
      $res = $this->logoutService->logoutAll($request->user());
      return ResponseFactory::ok($res);
    } catch(Exception $e) {
      return ResponseFactory::err(1001, 'Ha ocurrido un error');
    }
  }
}
