<?php

namespace App\Http\Controllers;

use App\Mail\InvitacionRegistroMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class InvitacionRegistroController extends Controller
{
  public function create(Request $request)
  {
    $invitacionRegistro = $this->createInvitacionRegistro(
      $this->getEmail($request), 
      $this->getVendedorId()
    );

    return Mail::to('naiger89@gmail.com')->send(new InvitacionRegistroMail('esto es un enlace'));
  }
}
