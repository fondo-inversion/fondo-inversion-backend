<?php

namespace App\Http\Controllers;

use App\Http\Responses\ResponseFactory;
use App\Services\LoginGranttypePassword\LoginGranttypePassword;
use App\Services\LoginGranttypePassword\LoginGranttypePasswordCmdFactory;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Services\Users\AuthUserProfile;

class LoginController extends Controller
{
    protected $loginGranttypePasswordCmdFactory;
    protected $loginGranttypePassword;
    protected $authUserProfile;

    public function __construct(
      AuthUserProfile $authUserProfile,
      LoginGranttypePassword $loginGranttypePassword,
      LoginGranttypePasswordCmdFactory $loginGranttypePasswordCmdFactory)
    {
      $this->loginGranttypePasswordCmdFactory = $loginGranttypePasswordCmdFactory;
      $this->loginGranttypePassword = $loginGranttypePassword;
      $this->authUserProfile = $authUserProfile;
    }

    public function login(Request $request)
    {
      try {
        $email = $request->get('email');
        $password = $request->get('password');
        $command = $this->loginGranttypePasswordCmdFactory->create('cbfx_client');
        $command->setCredentials($email, $password);
        $oauthResponse = $this->loginGranttypePassword->handle($command);
        $authProfile = $this->authUserProfile->profile($email);
        return ResponseFactory::ok(array_merge($oauthResponse, ['profile'=>$authProfile]));
      }

      catch(ClientException $e) {
        $responseErr = json_decode($e->getResponse()->getBody(), true);
        return ResponseFactory::err(1001, $responseErr['message']);
      }
    }
}
