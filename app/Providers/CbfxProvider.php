<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Database\Seeders\UserSeeder;
use App\Services\ProfileCrud\ProfileCrud;
use App\Services\ProfileCrud\PermissionProfile;
use App\Services\InvitacionRegistro\InvitacionRegistroFactory;
use App\Services\InvitacionRegistro\InvitacionRegistroService;
use App\Services\InvitacionRegistro\RegisterUserFromInvitacion;
use App\Services\Register\RegisterUserStandar;
use App\Services\Register\RegisterAdmin;
use App\Services\Register\RegisterVendedor;
use App\Services\RecuperarPassword\RecuperarPassword;
use App\Services\CreateSuperAdmin\CreateSuperAdmin;
use App\Services\Users\AuthUserProfile;
use App\Services\VerificacionEmail\VerficacionEmailService;
use App\Services\Logout\LogoutService;

class CbfxProvider extends ServiceProvider
{
  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton(PermissionProfile::class, function () {
      return new PermissionProfile();
    });

    $this->app->singleton(ProfileCrud::class, function () {
      return new ProfileCrud(
        $this->app->make(PermissionProfile::class)
      );
    });

    $this->app->singleton(AuthUserProfile::class, function () {
      return new AuthUserProfile();
    });

    $this->app->singleton(LogoutService::class, function () {
      return new LogoutService();
    });

    $this->app->singleton(VerficacionEmailService::class, function () {
      return new VerficacionEmailService();
    });

    $this->app->singleton(RegisterUserFromInvitacion::class, function () {
      return new RegisterUserFromInvitacion();
    });

    $this->app->singleton(InvitacionRegistroFactory::class, function () {
      return new InvitacionRegistroFactory();
    });

    $this->app->singleton(RecuperarPassword::class, function () {
      return new RecuperarPassword(
        $this->app->make(VerficacionEmailService::class)
      );
    });

    $this->app->singleton(RegisterUserStandar::class, function () {
      return new RegisterUserStandar(
        $this->app->make(VerficacionEmailService::class)
      );
    });

    $this->app->singleton(RegisterVendedor::class, function () {
      return new RegisterVendedor(
        $this->app->make(VerficacionEmailService::class)
      );
    });

    $this->app->singleton(RegisterAdmin::class, function () {
      return new RegisterAdmin(
        $this->app->make(VerficacionEmailService::class)
      );
    });

    $this->app->singleton(InvitacionRegistroService::class, function () {
      return new InvitacionRegistroService(
        $this->app->make(InvitacionRegistroFactory::class),
        $this->app->make(RegisterUserFromInvitacion::class)
      );
    });

    $this->app->singleton(CreateSuperAdmin::class, function () {
      return new CreateSuperAdmin(
        $this->app->make(RegisterUserStandar::class),
        $this->app->make(RegisterAdmin::class),
        $this->app->make(RegisterVendedor::class)
      );
    });

    $this->app->singleton(UserSeeder::class, function() {
      return new UserSeeder(
        $this->app->make(CreateSuperAdmin::class)
      );
    });    
  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot()
  {
    //
  }
}
