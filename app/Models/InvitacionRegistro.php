<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class InvitacionRegistro extends Model
{
  protected $table = 'invitacion_registros';
  protected $fillable  = [
    'usuario_id', 
    'usuario_detail_id', 
    'vendedor_id', 
    'profile_id', 
    'email_referido', 
    'is_register_ok', 
    'verification_hash'
  ];
  public $timestamps = false;

  public function profile() 
  {
    return $this->belongsTo(Profile::class, 'profile_id');
  }

  public function usuario() 
  {
    return $this->belongsTo(User::class, 'usuario_id');
  }

  public function usuarioDetail() 
  {
    return $this->belongsTo(UserDetail::class, 'usuario_detail_id');
  }

  public function vendedor() 
  {
    return $this->belongsTo(User::class, 'vendedor_id');
  }

  public function updateVerificationHash($verificactionHash)
  {
    $this->verification_hash = $verificactionHash;
  }

  public function verificationHash() 
  {
    return $this->verification_hash;
  }

  public function isRegisterOk()
  {
    return $this->is_register_ok;
  }

  public function emailReferido() 
  {
    return $this->email_referido;
  }

}
