<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DocumentoIdentidad extends Model
{
  use HasFactory;
  protected $table = 'documentos_identidad';
  protected $fillable  = [
    'usuario_id', 
    'usuario_detail_id', 
    'doc_id_front_photo', 
    'doc_id_reverse_photo', 
    'verified_at'
  ];
  public $timestamps = false;

}
