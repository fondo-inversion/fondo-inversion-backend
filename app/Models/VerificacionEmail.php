<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VerificacionEmail extends Model
{
  use HasFactory;

  protected $table = 'verificacion_emails';
  protected $fillable  = [
    'email', 
    'expired_at', 
    'verification_code', 
    'verification_hash',
    'is_used'
  ];
  public $timestamps = false;

  public function email() 
  {
    return $this->email;
  }

  public function isUsed()
  {
    return $this->is_used;
  }

  public function isExpired($now)
  {
    return $this->expired_at < $now;
  }

  public function verificationHash()
  {
    return $this->verification_hash;
  }

  public function verificationCode()
  {
    return $this->verification_code;
  }

  public function updateVerificationHash($verificationHash, $verificationCode, $expirationAt, $isUsed)
  {
    $this->fill([
      'verification_hash'=>$verificationHash,
      'verification_code'=>$verificationCode,
      'expired_at'=>$expirationAt,
      'is_used'=>$isUsed
    ]);
  }

  public function verify($verificationCode)
  {
    return $this->verification_code == $verificationCode;
  }

  public function complete()
  {
    $this->fill(['is_used'=>true]);
  }
}
