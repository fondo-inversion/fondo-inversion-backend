<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
  use HasFactory;
  protected $table = 'user_details';
  protected $fillable  = [
    'usuario_id',
    'photo',
    'email',
    'profile_id',
    'documento_identidad_id',
    'invitacion_registro_id',
    'register_at'
  ];
  public $timestamps = false;

  public function usuario()
  {
    return $this->belongsTo(User::class, 'usuario_id');
  }

  public function profile()
  {
    return $this->belongsTo(Profile::class, 'profile_id');
  }

  public function documentoIdentidad()
  {
    return $this->belongsTo(DocumentoIdentidad::class, 'documento_identidad_id');
  }

  public function invitacionRegistro()
  {
    return $this->belongsTo(InvitacionRegistro::class, 'invitacion_registro_id');
  }

  public function email()
  {
    return $this->usuario->email;
  }
}
