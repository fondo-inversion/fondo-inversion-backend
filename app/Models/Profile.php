<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  use HasFactory;
  protected $table = 'profiles';
  protected $fillable  = ['name', 'label', 'main_page', 'hash', 'permissions'];
  public $timestamps = false;

  public function sinPermisos()
  {
    return collect($this->toArray())->only(['main_page', 'hash']);
  }

  public function getPermissions() {
    return $this->permissions;
  }

  public function toList()
  {
    return [
      'id'=>$this->id,
      'name'=>$this->name,
      'label'=>$this->label,
      'hash'=>$this->hash,
      'mainPage'=>$this->main_page
    ];
  }

  public function toCache()
  {
    return [
      'hash'=>$this->hash,
      'permissions'=>$this->permissions
    ];
  }
}
