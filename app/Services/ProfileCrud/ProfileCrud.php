<?php

namespace App\Services\ProfileCrud;

use App\Models\Profile;
use Exception;
use Illuminate\Support\Facades\File;
use App\Services\ProfileCrud\PermissionProfile;
use \Webpatser\Uuid\Uuid;

class ProfileCrud
{
  protected $permissionProfile;
  public function __construct(PermissionProfile $permissionProfile)
  {
    $this->permissionProfile = $permissionProfile;
  }

  public function create($name, $label, $hash, $mainPage, $permissions)
  {
    $profile = Profile::create([
      'name' => $name,
      'label' => $label,
      'hash' => $hash,
      'main_page' => $mainPage,
      'permissions' => $permissions,
    ]);
    return $profile;
  }

  public function getPermissions($profileId)
  {
    $profile = Profile::find($id);
    if (!$profile) throw new Exception('ProfileNotFound');
    $permissionDecode = $this->permissionProfile->decodePermissionFromProfile($profile);
    return $permissionDecode; 
  }

  public function find($id)
  {
    $profile = Profile::find($id);
    if (!$profile) throw new Exception('ProfileNotFound');
    return $profile;
  }

  public function detail($id)
  {
    $profile = $this->find($id);
    return $profile->toList();
  }

  public function refresh()
  {
    Profile::all()->each(function($profile) {
      $this->update(
        $profile->id, 
        $profile->name, 
        $profile->label, 
        Uuid::generate()->string, 
        $profile->main_page, 
        $profile->permissions
      );
    });

    return $this->cache();
  }

  public function update($id, $name, $label, $hash, $mainPage, $permissions)
  {
    $profile = $this->find($id);
    $profile->fill([
      'name' => $name,
      'label' => $label,
      'hash' => $hash,
      'main_page' => $mainPage,
      'permissions' => $permissions
    ]);
    $profile->save();
    return $profile;
  }

  public function list()
  {
    return Profile::all()->map(function ($profile) {
      return $profile->toList();
    });
  }

  public function cache()
  {
    return Profile::all()->map(function ($profile) {
      return $profile->toCache();
    });
  }
}
