<?php

namespace App\Services\ProfileCrud;

use Illuminate\Support\Facades\File;

class PermissionProfile
{
  public function __construct()
  {
    
  }

  public function encodePermissionFromJsonFile($path)
  {
    $file = File::get($path);
    $decode = json_decode($file, true);
    $encode = json_encode($decode);
    return base64_encode($encode);
  }

  public function decodePermissionFromProfile($profile)
  {
    $permissions = $profile->getPermissions();
    $encode = base64_decode($permissions);
    $decode = json_decode($encode, true);
    return $decode;
  }

  public function makeTreePermissionFromArray($flatList) 
  {
    $grouped = [];
    foreach ($flatList as $node){
        $grouped[$node['parentId']][] = $node;
    }

    $fnBuilder = function($siblings) use (&$fnBuilder, $grouped) {
        foreach ($siblings as $k => $sibling) {
            $id = $sibling['id'];
            if(isset($grouped[$id])) {
                $sibling['children'] = $fnBuilder($grouped[$id]);
            }
            $siblings[$k] = $sibling;
        }
        return $siblings;
    };

    return $fnBuilder($grouped[0]);
  }
}