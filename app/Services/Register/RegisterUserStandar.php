<?php

namespace App\Services\Register;

use App\Models\Profile;
use App\Models\User;
use App\Services\VerificacionEmail\VerficacionEmailService;
use Illuminate\Support\Carbon;
use Exception;
use App\Models\UserDetail;
use App\Models\DocumentoIdentidad;

class RegisterUserStandar
{
  protected $verficacionEmailService;
  public function __construct(VerficacionEmailService $verficacionEmailService)
  {
    $this->verficacionEmailService = $verficacionEmailService;
  }

  public function verificarEmail($email, $sendHandler)
  {
    return $this->verificarEmailHandler($email, $sendHandler);
  }

  public function verificarEmailSinEnviarCorreo($email)
  {
    return $this->verificarEmailHandler($email, function($verificationEmail) {
      return $verificationEmail;
    });
  }

  protected function verificarEmailHandler($email, $sendHandler)
  {
    $this->verifyUserWithEmailOrFail($email);
    $expiredAt = Carbon::now()->addSeconds(180);
    return $verficacionEmail = $this->verficacionEmailService->send(
      $email, 
      $expiredAt->timestamp, 
      $sendHandler
    );
  }

  public function register($verificationHash, $verificationCode, $password)
  {
    $registerAt = Carbon::now()->timestamp;
    $verification = $this->verficacionEmailService->validation($verificationHash, $verificationCode, $registerAt);

    $email = $verification->email();
    $this->verifyUserWithEmailOrFail($email);

    $usuario = $this->createUsuario($email, $email, $password);
    $profile = $this->profileUser();
    $usuarioDetail = $this->createUsuarioDetailFromUsuario($usuario, $profile->id, $registerAt);
    $documentos = $this->createDocumentosIdentidadFromUsuarioDetail($usuario, $usuarioDetail);

    $usuarioDetail->load('usuario', 'profile', 'documentoIdentidad', 'invitacionRegistro');

    return $usuarioDetail;
  }

  public function verifyUserWithEmailOrFail($email)
  {
    $user = User::where('email', $email)->get()->first();
    if($user) throw new Exception("UserWithSameEmailException");
  }

  public function createUsuario($email, $name, $password)
  {
    $usuario = User::create([
      'email' => $email,
      'name' => $name,
      "password" => bcrypt($password)
    ]);

    return $usuario;
  }

  public function profileUser()
  {
    $profileUser = Profile::where('name', 'USUARIO')->get()->first();
    return $profileUser;
  }

  public function createUsuarioDetailFromUsuario($usuario, $profileId, $registerAt)
  {
    $usuarioDetail = UserDetail::create([
      'usuario_id'=>$usuario->id,
      //'photo'=>$usuario->id,
      'email'=>$usuario->email,
      'profile_id'=>$profileId,
      //'verificacion_email_id'=>$usuario->id,
      //'documento_identidad_id'=>$usuario->id,
      //'invitacion_registro_id'=>$invitacionRegistroId,
      'register_at'=>$registerAt
    ]);

    return $usuarioDetail;
  }

  public function createDocumentosIdentidadFromUsuarioDetail($user, $usuarioDetail)
  {
    $documentos = DocumentoIdentidad::create([
      'usuario_id'=>$user->id, 
      'usuario_detail_id'=>$usuarioDetail->id
    ]);
    $usuarioDetail->fill(['documento_identidad_id'=>$documentos->id]);
    $usuarioDetail->save();
  }
}