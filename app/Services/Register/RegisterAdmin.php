<?php

namespace App\Services\Register;
use App\Models\Profile;

class RegisterAdmin extends RegisterUserStandar
{
  public function profileUser()
  {
    $profileUser = Profile::where('name', 'ADMIN')->get()->first();
    return $profileUser;
  }
}