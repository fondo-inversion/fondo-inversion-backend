<?php

namespace App\Services\Register;
use App\Models\Profile;

class RegisterVendedor extends RegisterUserStandar
{
  public function profileUser()
  {
    $profileUser = Profile::where('name', 'VENDEDOR')->get()->first();
    return $profileUser;
  }
}