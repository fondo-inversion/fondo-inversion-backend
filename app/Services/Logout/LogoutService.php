<?php

namespace App\Services\Logout;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LogoutService
{
  public function __construct()
  {
  }

  public function logout(User $user)
  {
    $user = Auth::user()->token();
    return $user->revoke();
  }

  public function logoutAll(User $user)
  {
    $query = DB::table('oauth_access_tokens');
    $res = $query
      ->where('user_id', Auth::user()->id)
      ->update(['revoked' => true]);
    return $res;
  }
}
