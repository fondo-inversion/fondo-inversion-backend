<?php

namespace App\Services\VerificacionEmail;
use App\Models\VerificacionEmail;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Exception;

class VerficacionEmailService
{
  public function __construct()
  {
    
  }

  public function send($email, $expirationAt, $sendHandler)
  {
    if(!$email) throw new Exception("EmailRequiredException");
    
    $verification = VerificacionEmail::where('email', $email)->get()->first();
    $isUsed = false;

    if(!$verification) {
      $verification = VerificacionEmail::create([
        'email'=>$email,
        'expired_at'=>$expirationAt,
        'is_used'=>$isUsed
      ]);
    }

    $verificationHash = $this->makeVerificationHash();
    $codigoVerificacion = $this->makeCodigoVerificacion();
    $verification->updateVerificationHash($verificationHash, $codigoVerificacion, $expirationAt, $isUsed);
    $verification->save();

    return $sendHandler($verification);
  }

  public function makeVerificationHash()
  {
    return Uuid::generate()->string;
  }

  public function makeCodigoVerificacion()
  {
    return sprintf("%06d", mt_rand(1, 999999));
  }

  public function validation($verificationHash, $verificationCode, $now)
  {
    $verification = $this->findByVerificationHash($verificationHash);

    if($verification->isUsed()) throw new Exception("VerificationEmailUsedException");
    if($verification->isExpired($now)) throw new Exception("VerificationCodeExpiredException");
    if(!$verification->verify($verificationCode)) throw new Exception("VerificationCodeValidationException");

    $verification = $this->completeVerificationEmail($verification, $now);
    return $verification;
  }

  public function findByVerificationHash($verificationHash)
  {
    $verification = VerificacionEmail::where('verification_hash', $verificationHash)->get()->first();
    if(!$verification) throw new Exception("VerificacionEmailNotFound");
    return $verification;
  }

  public function completeVerificationEmail($verification, $now)
  {
    $verification->complete();
    $verification->save();

    return $verification;
  }
}