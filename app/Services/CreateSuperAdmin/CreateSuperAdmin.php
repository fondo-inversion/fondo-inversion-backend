<?php

namespace App\Services\CreateSuperAdmin;

use App\Models\Profile;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\DocumentoIdentidad;
use App\Services\Register\RegisterAdmin;
use Carbon\Carbon;
use App\Services\Register\RegisterUserStandar;
use App\Services\Register\RegisterVendedor;
use \Webpatser\Uuid\Uuid;

class CreateSuperAdmin
{
  protected $registerUserStandar;
  protected $registerAdmin;
  protected $registerVendedor;
  public function __construct(
    RegisterUserStandar $registerUserStandar, 
    RegisterAdmin $registerAdmin, 
    RegisterVendedor $registerVendedor) {

    $this->registerUserStandar = $registerUserStandar;
    $this->registerAdmin = $registerAdmin;
    $this->registerVendedor = $registerVendedor;
  }

  public function create()
  {
    $profiles = $this->createProfiles();
    
    $verification = $this->registerAdmin->verificarEmailSinEnviarCorreo('naiger67@gmail.com');
    $admin = $this->registerAdmin->register($verification->verificationHash(), $verification->verificationCode(), '12345678.');

    $verification = $this->registerVendedor->verificarEmailSinEnviarCorreo('naiger68@gmail.com');
    $vendedor = $this->registerVendedor->register($verification->verificationHash(), $verification->verificationCode(), '12345678.');

    $verification = $this->registerUserStandar->verificarEmailSinEnviarCorreo('naiger69@gmail.com');
    $user = $this->registerUserStandar->register($verification->verificationHash(), $verification->verificationCode(), '12345678.');

    return compact('admin', 'vendedor', 'user', 'profiles');
  }

  protected function createProfiles()
  {
    $profiles = collect([])
      ->push($this->createProfile('ADMIN', 'Administrador', Uuid::generate()->string, '/admin-profile'))
      ->push($this->createProfile('VENDEDOR', 'Vendedor', Uuid::generate()->string, '/admin-profile'))
      ->push($this->createProfile('USUARIO', 'Usuario', Uuid::generate()->string, '/admin-profile'));

    return $profiles;
  }

  protected function createProfile($name, $label, $hash, $mainPage)
  {
    $profile = Profile::create([
      'name'=>$name,
      'label'=>$label,
      'hash'=>$hash,
      'main_page'=>$mainPage,
      'permissions'=>'',
    ]);

    return $profile;
  }
}
