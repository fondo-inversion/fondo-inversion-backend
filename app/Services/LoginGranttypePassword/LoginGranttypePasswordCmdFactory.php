<?php

namespace App\Services\LoginGranttypePassword;

use App\Services\LoginGranttypePassword\LoginGranttypePasswordCmd;
use Exception;

class LoginGranttypePasswordCmdFactory
{

  public function __construct()
  {
  }

  public function create($clientAlias): LoginGranttypePasswordCmd
  {
    $command;
    switch ($clientAlias) {
      case 'cbfx_client':
        $command = $this->command($clientAlias);
        break;
      default:
        throw new Exception("ClientNotFoundException", 1);
        break;
      return $command;
    }
    return $command;
  }

  protected function command($clientAlias)
  {
    $client = $this->client($clientAlias);
    $command = new LoginGranttypePasswordCmd($client['id'], $client['secret'], $client['scope']);
    return $command;
  }

  protected function client($alias)
  {
    $clientId = config('oauth2.clients.'.$alias.'.client_id');
    $clientSecret = config('oauth2.clients.'.$alias.'.client_secret');
    $scope = config('oauth2.clients.'.$alias.'.scope');
    return [
      'id'=>$clientId,
      'secret'=>$clientSecret,
      'scope'=>join(' ', $scope)
    ];
  }
}
