<?php

namespace App\Services\LoginGranttypePassword;
use App\Services\LoginGranttypePassword\LoginGranttypePasswordCmd;
use Exception;
use GuzzleHttp\Exception\ClientException;

class LoginGranttypePassword
{
  protected $url;

  public function __construct()
  {
    $this->url = config('oauth2.url');
  }

  protected function http()
  {
    return new \GuzzleHttp\Client;
  }

  protected function oauth2Token()
  {
    return $this->url . "oauth/token";
  }

  public function handle(LoginGranttypePasswordCmd $command)
  {
    $response = $this->http()->post($this->oauth2Token(), [
      'form_params' => $this->command($command),
    ]);
    return json_decode((string) $response->getBody(), true);
  }

  protected function command(LoginGranttypePasswordCmd $command)
  {
    return [
      'grant_type' => 'password',
      'client_id' => $command->getClientId(),
      'client_secret' => $command->getClientSecret(),
      'username' => $command->getUsername(),
      'password' => $command->getPassword(),
      'scope' => $command->getScope(),
    ];
  }
}
