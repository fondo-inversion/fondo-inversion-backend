<?php

namespace App\Services\LoginGranttypePassword;

class LoginGranttypePasswordCmd
{
  protected $clientId;
  protected $clientSecret;
  protected $scope;
  protected $email;
  protected $password;

  public function __construct($clientId, $clientSecret, $scope)
  {
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->scope = $scope;
  }

  public function setCredentials($email, $password)
  {
    $this->email = $email;
    $this->password = $password;
  }

  public function getClientId()
  {
    return $this->clientId;
  }
  
  public function getClientSecret()
  {
    return $this->clientSecret;
  }

  public function getScope()
  {
    return $this->scope;
  }

  public function getUsername()
  {
    return $this->email;
  }
  
  public function getPassword()
  {
    return $this->password;
  }

  public function toArray()
  {
    return [
      'clientId'=>$this->clientId,
      'clientSecret'=>$this->clientSecret,
      'scope'=>$this->scope,
      'email'=>$this->email,
      'password'=>$this->password,
    ];
  }
}