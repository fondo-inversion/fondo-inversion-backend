<?php

namespace App\Services\InvitacionRegistro;

use App\Models\InvitacionRegistro;
use App\Models\Profile;
use App\Models\User;
use Webpatser\Uuid\Uuid;
use Exception;

class InvitacionRegistroFactory
{
  public function __construct()
  {

  }

  public function make($email, $registerId, $profileId, $verficationHash)
  {
    $invitacionRegistro = $this->findInvitacionByEmailFail($email, $registerId);
    if (!$invitacionRegistro) {
      $invitacionRegistro = $this->create($email, $registerId, $profileId, $verficationHash);
      return $invitacionRegistro;
    };

    $invitacionRegistro->updateVerificationHash($verficationHash);
    $invitacionRegistro->save();
    return $invitacionRegistro;
  }

  protected function findInvitacionByEmailFail($email, $registerId)
  {
    $invitacionRegistro = $this->whereByCreate($email, $registerId);
    if (!$invitacionRegistro) return;
    
    if ($invitacionRegistro->isRegisterOk()) throw new Exception("InvitacionRegistroWasUsedException");
    return $invitacionRegistro;
  }

  protected function whereByCreate($email, $registerId)
  {
    return InvitacionRegistro::where('email_referido', $email)
      ->where('vendedor_id', $registerId)->get()->first();
  }

  protected function create($email, $registerId, $profileId, $verficationHash)
  {
    $register = $this->registerOrFail($registerId);
    $profile = $this->profileOrFail($profileId);

    $invitacionRegistro = InvitacionRegistro::create([
      'vendedor_id'=>$register->id,
      'profile_id'=>$profile->id,
      'email_referido'=>$email,
      'is_register_ok'=>false,
      'verification_hash'=>$verficationHash
    ]);

    return $invitacionRegistro;
  }

  protected function registerOrFail($id)
  {
    $register = User::find($id);
    if(!$register) throw new Exception("RegisterNotFoundException");
    return $register;
  }

  protected function profileOrFail($id)
  {
    $profile = Profile::find($id);
    if(!$profile) throw new Exception("ProfileNotFoundException");
    return $profile;
  }
}
