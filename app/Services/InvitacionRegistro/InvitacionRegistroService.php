<?php

namespace App\Services\InvitacionRegistro;

use App\Models\DocumentoIdentidad;
use App\Models\InvitacionRegistro;
use App\Models\Profile;
use App\Models\User;
use App\Models\UserDetail;
use App\Services\Register\RegisterUserFromInvitacion;
use \Webpatser\Uuid\Uuid;
use Exception;

class InvitacionRegistroService
{
  protected $registerUserFromInvitacion;
  protected $invitacionRegistroFactory;

  public function __construct(
    InvitacionRegistroFactory $invitacionRegistroFactory,
    RegisterUserFromInvitacion $registerUserFromInvitacion)
  {
    $this->registerUserFromInvitacion = $registerUserFromInvitacion;
    $this->invitacionRegistroFactory = $invitacionRegistroFactory;
  }

  public function send($email, $profileId, $registerId, $sendHandler)
  {
    $invitacion = $this->invitacionRegistroFactory->make($email, $profileId, $registerId)
    return $sendHandler($invitacion);
  }

  public function register($verificactionHash, $password, $registerAt)
  {
    $usuarioDetail = $this->registerUserFromInvitacion->register($verificactionHash, $password, $registerAt);
    $usuarioDetail->load('usuario', 'profile', 'documentoIdentidad', 'invitacionRegistro');
    return $usuarioDetail;
  }

}
