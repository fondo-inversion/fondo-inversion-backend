<?php

namespace App\Services\InvitacionRegistro;

use Exception;
use App\Models\User;
use App\Models\UserDetail;
use App\Models\DocumentoIdentidad;
use App\Models\InvitacionRegistro;

class RegisterUserFromInvitacion
{
  public function __construct()
  {
    
  }

  public function register($verificationHash, $password, $registerAt)
  {
    $invitacion = $this->invitacionOrFail($verificationHash);
    $this->findUsuarioByEmailFail($invitacion->emailReferido());
    $usuario = $this->createUsuario($invitacion->emailReferido(), $invitacion->emailReferido(), $password);
    $profile = $invitacion->profile;
    $usuarioDetail = $this->createUsuarioDetailFromUsuario($usuario, $profile->id, $invitacion->id, $registerAt);
    $documentos = $this->createDocumentosIdentidadFromUsuarioDetail($usuario, $usuarioDetail);
    $this->bindInvitacionRegistro($invitacion, $usuario, $usuarioDetail, $registerAt);
    return $usuarioDetail;
  }

  protected function invitacionOrFail($verificationHash)
  {
    $invitacionRegistro = InvitacionRegistro::where('verification_hash', $verificationHash)->get()->first();
    if(!$invitacionRegistro) throw new Exception("InvitacionRegistroNotFoundException");
    if($invitacionRegistro->isRegisterOk()) throw new Exception("InvitacionRegistroWasUsedException");
    return $invitacionRegistro;
  }

  public function findUsuarioByEmailFail($email)
  {
    $usuario = User::where('email', $email)->get()->first();
    if ($usuario) throw new Exception("UserHasRegisterException");
    return $usuario;
  }

  public function createUsuario($email, $name, $password)
  {
    $usuario = User::create([
      'email' => $email,
      'name' => $name,
      "password" => bcrypt($password)
    ]);

    return $usuario;
  }

  public function createUsuarioDetailFromUsuario($usuario, $profileId, $invitacionRegistroId, $registerAt)
  {
    $usuarioDetail = UserDetail::create([
      'usuario_id'=>$usuario->id,
      //'photo'=>$usuario->id,
      'email'=>$usuario->email,
      'profile_id'=>$profileId,
      //'verificacion_email_id'=>$usuario->id,
      //'documento_identidad_id'=>$usuario->id,
      'invitacion_registro_id'=>$invitacionRegistroId,
      'register_at'=>$registerAt
    ]);

    return $usuarioDetail;
  }

  public function createDocumentosIdentidadFromUsuarioDetail($user, $usuarioDetail)
  {
    $documentos = DocumentoIdentidad::create([
      'usuario_id'=>$user->id, 
      'usuario_detail_id'=>$usuarioDetail->id
    ]);
    $usuarioDetail->fill(['documento_identidad_id'=>$documentos->id]);
    $usuarioDetail->save();
  }

  public function bindInvitacionRegistro($invitacionRegistro, $user, $userDetail, $registerAt)
  {
    $invitacionRegistro->fill([
      'usuario_id'=>$user->id, 
      'usuario_detail_id'=>$userDetail->id,
      'is_register_ok'=>true 
    ]);

    $invitacionRegistro->save();
  }

  
}