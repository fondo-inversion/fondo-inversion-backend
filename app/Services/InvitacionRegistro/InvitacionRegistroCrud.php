<?php

namespace App\Services\InvitacionRegistro;

use App\Models\InvitacionRegistro;
use Exception;

class InvitacionRegistroCrud
{
  public function __construct()
  {

  }

  public function detail($id)
  {
    $relations = ['profile','usuario','usuarioDetail','vendedor'];
    $invitacionRegistro = InvitacionRegistro::with($relations)->find($id);
    if(!$invitacionRegistro) throw new Exception("InvitacionRegistroNotFoundException");
    return $invitacionRegistro;
  }

  public function list()
  {
    return InvitacionRegistro::with($relations)->get();
  }

}