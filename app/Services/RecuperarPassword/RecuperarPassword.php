<?php

namespace App\Services\RecuperarPassword;

use App\Services\VerificacionEmail\VerficacionEmailService;
use App\Models\User;
use Illuminate\Support\Carbon;
use Exception;

class RecuperarPassword
{
  public $verficacionEmailService;
  public function __construct(VerficacionEmailService $verficacionEmailService)
  {
    $this->verficacionEmailService = $verficacionEmailService;
  }

  public function verificationEmail($email)
  {
    $this->findUserOrFail($email);

    $expiredAt = Carbon::now()->addSeconds(180);
    return $verficacionEmail = $this->verficacionEmailService->send(
      $email, 
      $expiredAt->timestamp, 
      function($verificationEmail) {
        return $verificationEmail;
      }
    );
  }

  public function recover($verificationHash, $verificationCode, $newPassword)
  {
    $recoverAt = Carbon::now()->timestamp;
    $verification = $this->verficacionEmailService->validation($verificationHash, $verificationCode, $recoverAt);
    $email = $verification->email();
    $user = $this->findUserOrFail($email);
    $user->changePassword(bcrypt($newPassword));
    $user->save();

    return $user;
  }

  public function findUserOrFail($email)
  {
    $user = User::where('email', $email)->get()->first();
    if(!$user) throw new Exception("UserNotFound");
    return $user;
  }

}