<?php

namespace App\Services\Users;

use App\Models\User;
use App\Models\UserDetail;
use Exception;

class AuthUserProfile
{
  public function profile($email)
  {
    $user = User::where('email', $email)->get()->first();
    if(!$user) throw new Exception("UserNotfoundException");
    $profile = $user->userDetail->profile->sinPermisos();
    return $profile;
  }
}